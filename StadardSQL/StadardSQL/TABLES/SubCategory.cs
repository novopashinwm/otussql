using System.ComponentModel.DataAnnotations;

namespace OtusSQL.TABLES
{
    public class SubCategory
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public Category Category { get; set; }
    }
}