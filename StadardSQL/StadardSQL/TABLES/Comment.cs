using System.ComponentModel.DataAnnotations;

namespace OtusSQL.TABLES
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public string Text { get; set; }
    }
}