﻿namespace StadardSQL
{
    using OtusSQL.TABLES;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class CodeContext : DbContext
    {
        // Контекст настроен для использования строки подключения "CodeContext" из файла конфигурации  
        // приложения (App.config или Web.config). По умолчанию эта строка подключения указывает на базу данных 
        // "StadardSQL.CodeContext" в экземпляре LocalDb. 
        // 
        // Если требуется выбрать другую базу данных или поставщик базы данных, измените строку подключения "CodeContext" 
        // в файле конфигурации приложения.
        public CodeContext()
            : base("name=CodeContext")
        {
        }

        // Добавьте DbSet для каждого типа сущности, который требуется включить в модель. Дополнительные сведения 
        // о настройке и использовании модели Code First см. в статье http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<SubCategory> SubCategories { get; set; }
    }

}